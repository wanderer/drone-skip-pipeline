package main

import (
	// "encoding/json"
	"errors"
	"fmt"
	"log"

	"code.gitea.io/sdk/gitea"
	"github.com/kelseyhightower/envconfig"
)

type config struct {
	Event     string   `envconfig:"DRONE_BUILD_EVENT"`
	Commit    string   `envconfig:"DRONE_COMMIT"`
	RepoName  string   `envconfig:"DRONE_REPO_NAME"`
	RepoOwner string   `envconfig:"DRONE_REPO_OWNER"`
	RemoteURL string   `envconfig:"DRONE_REMOTE_URL"`
	Rules     []string `envconfig:"PLUGIN_RULES"`
}

type modifiedFile struct {
	Filename string
}

func main() {
	client, err := gitea.NewClient("https://git.dotya.ml")
	if err != nil {
		log.Println(err)
	}

	cfg, err := loadConfig()
	if err != nil {
		log.Println(err)
	}

	currentCommit, _, err := client.GetSingleCommit(
		cfg.RepoOwner,
		cfg.RepoName,
		cfg.Commit,
	)

	commitFiles := currentCommit.Files

	var files []modifiedFile

	nufiles := make([]modifiedFile, len(commitFiles), (cap(commitFiles)+1)*2)

	for _, i := range commitFiles {
		fmt.Println(i.Filename)
	}

	for i := range commitFiles {
		files[i] = nufiles[i]
	}
	nufiles = files

	// err = json.Unmarshal(commitFiles, &files)
	// var v []string
	// json.Unmarshal(commitFiles, v)

	// fmt.Println(currentCommit.Files)
}

func mainCmd() error { return nil }

func loadConfig() (*config, error) {
	var cfg config
	err := envconfig.Process("", &cfg)
	if err != nil {
		return nil, err
	}

	switch {
	case cfg.Event == "":
		return nil, errors.New("missing DRONE_BUILD_EVENT")
	case cfg.Commit == "":
		return nil, errors.New("missing DRONE_COMMIT")
	case cfg.RepoName == "":
		return nil, errors.New("missing DRONE_REPO_NAME")
	case cfg.RepoOwner == "":
		return nil, errors.New("missing DRONE_REPO_OWNER")
	case cfg.RemoteURL == "":
		return nil, errors.New("missing DRONE_REMOTE_URL")
	}

	return &cfg, nil
}

// atm ran like this:
// DRONE_BUILD_EVENT="push" DRONE_COMMIT="7121d8f91a" DRONE_REPO_NAME="go-xkcdreader" DRONE_REPO_OWNER="wanderer" DRONE_REMOTE_URL="https://git.dotya.ml/wanderer/go-xkcdreader.git" gor -v.
