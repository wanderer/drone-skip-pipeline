#!/bin/bash

export DRONE_BUILD_EVENT="push"
export DRONE_COMMIT="7121d8f91a"
export DRONE_REPO_NAME="go-xkcdreader"
export DRONE_REPO_OWNER="wanderer"
export DRONE_REMOTE_URL="https://git.dotya.ml/wanderer/go-xkcdreader.git"
export DRONE_COMMIT_LINK="https://git.dotya.ml/wanderer/go-xkcdreader.git/pull/1"

go run -v .
