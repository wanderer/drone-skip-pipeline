# `drone-skip-pipeline`

this program is supposed to work as a DroneCI plugin.
* skips pipelines if files/patterns listed are not matched.
* works on commits
* works on arbitrary commits, not just PRs
* works with any Gitea instance

heavily inspired by [joshdk's drone-skip-pipeline](https://github.com/joshdk/drone-skip-pipeline/), except this plugin only works with Gitea instances, whereas the former only worked with GitHub.

### License
CC0
