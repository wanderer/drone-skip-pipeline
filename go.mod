module git.dotya.ml/wanderer/drone-skip-pipeline

go 1.18

require (
	code.gitea.io/sdk/gitea v0.15.1
	github.com/kelseyhightower/envconfig v1.4.0
)

require github.com/hashicorp/go-version v1.2.1 // indirect
